CREATE TABLE IF NOT EXISTS `user` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `emailId` VARCHAR(45) NOT NULL,
  `user_name` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `location` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS  `user_role` (
  `user_role_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`user_role_id`,`user_id`),
  INDEX `user_id_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `role_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES  `user` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `role_id_fk`
    FOREIGN KEY (`user_role_id`)
    REFERENCES  `role` (`role_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS  `verticle` (
  `verticle_id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `business_unit` VARCHAR(45) NOT NULL,
  `resources` INT NULL,
  PRIMARY KEY (`verticle_id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS  `role` (
  `role_id` INT NOT NULL,
  `role_name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NULL,
  PRIMARY KEY (`role_id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS  `score_metric` (
  `score_metric_id` INT NOT NULL AUTO_INCREMENT,
  `agile_focusarea` VARCHAR(200) NOT NULL,
  `agile_metric` VARCHAR(200) NOT NULL,
  `metric_options` VARCHAR(200) NOT NULL,
  `score_metriccol` VARCHAR(45) NOT NULL,
  `score` INT NOT NULL,
  `stream` VARCHAR(45) NOT NULL,
  `score_metriccol1` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`score_metric_id`))
ENGINE = InnoDB;



CREATE TABLE IF NOT EXISTS  `account_portfolio` (
  `account_portfolio_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `business_unit` VARCHAR(45) NOT NULL,
  `resources` INT NULL,
  `verticle_id` INT NOT NULL,
  `account_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`account_portfolio_id`),
  INDEX `verticle_id_idx` (`verticle_id` ASC) VISIBLE,
  CONSTRAINT `verticle_id`
    FOREIGN KEY (`verticle_id`)
    REFERENCES  `verticle` (`verticle_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS  `project_portfolio` (
  `project_portfolio_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `resources` INT NOT NULL,
  `business_unit` VARCHAR(45) NOT NULL,
  `account_portfolio_id` INT NOT NULL,
  PRIMARY KEY (`project_portfolio_id`),
  INDEX `account_portfolio_id_idx` (`account_portfolio_id` ASC) VISIBLE,
  CONSTRAINT `account_portfolio_id_fk`
    FOREIGN KEY (`account_portfolio_id`)
    REFERENCES  `account_portfolio` (`account_portfolio_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS  `sprint_team` (
  `sprint_team_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `resources` VARCHAR(45) NULL,
  `business_unit` VARCHAR(45) NOT NULL,
  `project_portfolio_id` INT NOT NULL,
  PRIMARY KEY (`sprint_team_id`),
  INDEX `project_portfolio_id_idx` (`project_portfolio_id` ASC) VISIBLE,
  CONSTRAINT `project_portfolio_id_fk`
    FOREIGN KEY (`project_portfolio_id`)
    REFERENCES  `project_portfolio` (`project_portfolio_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;




CREATE TABLE IF NOT EXISTS  `audit_log` (
  `audit_log_id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `change_log` VARCHAR(200) NOT NULL,
  `event_date` DATETIME NOT NULL,
  PRIMARY KEY (`audit_log_id`),
  INDEX `user_id_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `user_id_fk`
    FOREIGN KEY (`user_id`)
    REFERENCES  `user` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS  `user_assignment` (
  `user_assignment_id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `sprint_team_id` INT NOT NULL,
  `business_unit` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`user_assignment_id`),
  INDEX `sprint_team_id_idx` (`sprint_team_id` ASC) VISIBLE,
  INDEX `user_id_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `sprint_team_id_fk`
    FOREIGN KEY (`sprint_team_id`)
    REFERENCES  `sprint_team` (`sprint_team_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `user_id_fk`
    FOREIGN KEY (`user_id`)
    REFERENCES  `user` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;




CREATE TABLE IF NOT EXISTS  `sprint_team` (
  `sprint_team_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `resources` VARCHAR(45) NULL,
  `business_unit` VARCHAR(45) NOT NULL,
  `project_portfolio_id` INT NOT NULL,
  PRIMARY KEY (`sprint_team_id`),
  INDEX `project_portfolio_id_idx` (`project_portfolio_id` ASC) VISIBLE,
  CONSTRAINT `project_portfolio_id_fk`
    FOREIGN KEY (`project_portfolio_id`)
    REFERENCES  `project_portfolio` (`project_portfolio_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS  `sprints` (
  `sprints_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `start_date` DATETIME NOT NULL,
  `end_date` DATETIME NOT NULL,
  `capacity` INT NOT NULL,
  `velocity` INT NOT NULL,
  `story_point_planned` INT NULL,
  `story_point_completed` INT NULL,
  `sprint_team_is` INT NOT NULL,
  PRIMARY KEY (`sprints_id`),
  CONSTRAINT `sprint_team_id_sprintsfk`
    FOREIGN KEY (`sprints_id`)
    REFERENCES  `sprint_team` (`sprint_team_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS  `scorecard` (
  `scorecard_id` INT NOT NULL AUTO_INCREMENT,
  `sprints_id` INT NULL,
  `org_support` DECIMAL(2) NULL,
  `agile_quality_advocacy` DECIMAL(2) NULL,
  `agile_product_ownership` DECIMAL(2) NULL,
  `team_dynamic_environment` DECIMAL(2) NULL,
  `agile_engineering` DECIMAL(2) NULL,
  `overall_score` DECIMAL(2) NULL,
  `score_details` JSON NULL,
  PRIMARY KEY (`scorecard_id`),
  INDEX `sprints_id_idx` (`sprints_id` ASC) VISIBLE,
  CONSTRAINT `sprints_id_fk`
    FOREIGN KEY (`sprints_id`)
    REFERENCES  `sprints` (`sprints_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



CREATE TABLE IF NOT EXISTS `sprint_score` (
  `sprint_score_id` INT NOT NULL AUTO_INCREMENT,
  `sprints_id` INT NOT NULL,
  `score_metric_id` INT NOT NULL,
  PRIMARY KEY (`sprint_score_id`),
  INDEX `score_metric_id_idx` (`score_metric_id` ASC) VISIBLE,
  INDEX `sprints_id_idx` (`sprints_id` ASC) VISIBLE,
  CONSTRAINT `score_metric_id_fk`
    FOREIGN KEY (`score_metric_id`)
    REFERENCES `score_metric` (`score_metric_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `sprints_id_scorefk`
    FOREIGN KEY (`sprints_id`)
    REFERENCES `sprints` (`sprints_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



