/*
-- Query: select * from PROJECT_MANAGER_MAP
LIMIT 0, 1000

-- Date: 2023-01-11 19:20
*/
INSERT INTO `PROJECT_MANAGER_MAP` (`id`,`project`,`Onsite_PM`,`Offshore_PM`,`Delivery_Lead`,`Portfolio_Lead`) VALUES (1,'Utopia',NULL,'Shina Liju','Vivek Venu Shenoy','Jithesh Nakulan');
INSERT INTO `PROJECT_MANAGER_MAP` (`id`,`project`,`Onsite_PM`,`Offshore_PM`,`Delivery_Lead`,`Portfolio_Lead`) VALUES (2,'Utilization',NULL,'Shilna Liju','Vivek Venu Shenoy','Jithesh Nakulan');
INSERT INTO `PROJECT_MANAGER_MAP` (`id`,`project`,`Onsite_PM`,`Offshore_PM`,`Delivery_Lead`,`Portfolio_Lead`) VALUES (3,'API Explorer','Neha Prabhakar',NULL,'Jaheer Sainulabdeen Laila','Raju Haldar');
INSERT INTO `PROJECT_MANAGER_MAP` (`id`,`project`,`Onsite_PM`,`Offshore_PM`,`Delivery_Lead`,`Portfolio_Lead`) VALUES (4,'CDP','Alex Fishman','Vinil Vijayan','Sajunath Somanathan Nair','Raju Haldar');
INSERT INTO `PROJECT_MANAGER_MAP` (`id`,`project`,`Onsite_PM`,`Offshore_PM`,`Delivery_Lead`,`Portfolio_Lead`) VALUES (5,'Pacbot',NULL,'Surendranath Tirunagaram','Vivek Venu Shenoy','Jithesh Nakulan');
INSERT INTO `PROJECT_MANAGER_MAP` (`id`,`project`,`Onsite_PM`,`Offshore_PM`,`Delivery_Lead`,`Portfolio_Lead`) VALUES (6,'API Security','Madhumalar Panneerselvam','','Jaheer Sainulabdeen Laila','Raju Haldar');
INSERT INTO `PROJECT_MANAGER_MAP` (`id`,`project`,`Onsite_PM`,`Offshore_PM`,`Delivery_Lead`,`Portfolio_Lead`) VALUES (7,'AppSec','Rajesh George',NULL,'Jaheer Sainulabdeen Laila','Roopesh Rajendran');
INSERT INTO `PROJECT_MANAGER_MAP` (`id`,`project`,`Onsite_PM`,`Offshore_PM`,`Delivery_Lead`,`Portfolio_Lead`) VALUES (8,'SharePoint',NULL,'Kavya Kamal','Raju Haldar','Raju Haldar');
INSERT INTO `PROJECT_MANAGER_MAP` (`id`,`project`,`Onsite_PM`,`Offshore_PM`,`Delivery_Lead`,`Portfolio_Lead`) VALUES (9,'Prism',NULL,'Kiran Krishnan','Vivek Venu Shenoy','Jithesh Nakulan');
